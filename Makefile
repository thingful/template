# Simple Makefile for building and testing library
#
ARTEFACT_DIR = ./build
GOCMD = go
GOTEST = $(GOCMD) test -v
GORUN = $(GOCMD) run
GOLINT = gometalinter --vendor --disable-all --enable=vet --enable=golint --enable=errcheck --enable=misspell
GOCOVER = $(GOCMD) tool cover
GLIDE = glide
PACKAGE_NAME = $(notdir $(shell pwd))
DATE_TIME = $(shell date -u "+%Y-%m-%dT%H-%M-%SZ")
LOG_FILE = $(ARTEFACT_DIR)/$(PACKAGE_NAME).$(DATE_TIME).log

help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "  html       run tests and generate HTML coverage report into build/coverage.html"
	@echo "  coverage   run tests and output coverage info to the console"
	@echo "  setup      install gometalinter"
	@echo "  update     run glide update"
	@echo "  install    run glide install"
	@echo "  test       run tests"
	@echo "  lint       run gometalinter on the code"
	@echo "  clean      remove all generated artefacts, i.e. coverage reports"
	@echo "  full       run tests and output both console and html coverage reports"
	@echo "  harness    run testharness, getting URLs and Fetch for real"
default: help

.PHONY: setup
setup:
	$(GOCMD) get -u github.com/alecthomas/gometalinter
	gometalinter --install

.PHONY: update
update:
	$(GLIDE) update

.PHONY: install
install:
	$(GLIDE) install

.PHONY: test
test:
	mkdir -p $(ARTEFACT_DIR)
ifdef TEST
	$(GOTEST) -coverprofile=$(ARTEFACT_DIR)/cover.out -run ${TEST}
else
	$(GOTEST) -coverprofile=$(ARTEFACT_DIR)/cover.out
endif

.PHONY: coverage
coverage: test
	mkdir -p $(ARTEFACT_DIR)
	$(GOCOVER) -func=$(ARTEFACT_DIR)/cover.out

.PHONY: html
html: test
	mkdir -p $(ARTEFACT_DIR)
	$(GOCOVER) -html=$(ARTEFACT_DIR)/cover.out -o $(ARTEFACT_DIR)/coverage.html

.PHONY: lint
lint:
	$(GOLINT) ./...

.PHONY: clean
clean:
	rm -rf $(ARTEFACT_DIR)

full: test coverage html

.PHONY: harness
harness:
	mkdir -p $(ARTEFACT_DIR)
	$(GORUN) cmd/harness/main.go | tee $(LOG_FILE)

