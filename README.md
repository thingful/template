# Template

This project contains a template fetcher for getting data into Thingful.

## Type of fetch

This fetcher is for the case of relatively simple upstream provider where the
fetcher simply has to make a single request to get some data, then parse it and
give back a slice of Things to Thingful.

Please see the other published branches in this repository for examples that:

* require authentication credentials

* require sending POST requests

* require making mulitple requests to fully populate the returned data.


## Getting started

* Copy the whole project to some new folder named for the upstream provider you
  want to index

* Change all package names to something that makes sense for the new provider

* Start working on  `fetcher.go` and corresponding `fetcher_test.go` such hat
  it is able to go fetch data from the specified upstream provider

* Implement a sensible `NewFetcher` implementation in `fetcher.go` so that it
  instantiates and returns a correctly instantiated `Fetcher`.

* Push the project up to Bitbucket under the Thingful organisation namespace.

* Add the project to our CI tool (Wercker in current incarnation)

## Copyright

Copyright © Thingful Ltd 2016.

