// Package template is a template implementation for an example crawler for
// Thingful. This particular version is a very simple implementation of a
// fetcher that requires no unusual steps in order to retrieve data.
package template

import (
	"context"
	"time"

	"github.com/thingful/thingfulx"
)

const (

	// IndexerUID is the name of this fetcher,
	// this should reflect either the name of the provider or the attribute of this dataset
	IndexerUID = "template"

	// ProviderURL is url to the data provider's homepage
	ProviderURL = "http://example.com/"
)

// NewIndexer instantiates a new Fetcher instance. All fetchers must try to
// load any required config values from namespaced environment variables.
func NewIndexer() (thingfulx.Indexer, error) {
	// we know these urls won't give an error

	provider := &thingfulx.Provider{
		Name:        "provider name",
		ID:          IndexerUID,
		URL:         ProviderURL,
		Description: "a brief description of the data provider",
	}

	return &indexer{
		provider: provider,
	}, nil
}

// indexer is our indexer type
type indexer struct {
	provider *thingfulx.Provider
}

// UID return the unique indentifier of this indexer
func (i *indexer) UID() string {
	return IndexerUID
}

// URLS returns the minimum set of urls required to fully index this data provider
func (i *indexer) URLS(ctx context.Context, client thingfulx.Client, delay time.Duration) ([]string, error) {
	return []string{"http://example.com/foo"}, nil
}

// Fetch a resource from the upstream provider if we can.
func (i *indexer) Fetch(ctx context.Context, url string, client thingfulx.Client) ([]byte, error) {
	return client.DoHTTPGetRequest(url)
}

// Parse is our function to actually extract data and return the slice of things. we should
func (i *indexer) Parse(data []byte, urlStr string, timeProvider thingfulx.TimeProvider) ([]thingfulx.Thing, error) {
	things := []thingfulx.Thing{}

	if string(data) == "valid-data" {
		return things, nil
	}

	return things, thingfulx.NewErrBadData("bad data")
}
