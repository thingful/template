// +build harness

package main

import (
	"bitbucket.org/thingful/template"
	"github.com/thingful/testharness"
	"golang.org/x/net/context"
	"time"
)

var URLs []string
var err error

func main() {

	harness, err := testharness.Register(template.NewIndexer, false)

	if err != nil {
		panic(err)
	}

	// run everything, Provider, URLs and Fetch
	fetchAllInterval := time.Duration(5) * time.Second // interval between each Fetch
	urlsToFetch := 10                                  // total of urls to Fetch
	harness.RunAll(context.Background(), fetchAllInterval, urlsToFetch)
}
