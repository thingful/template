package template

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/thingful/httpmock"
	"github.com/thingful/thingfulx"
)

func TestNewIndexer(t *testing.T) {
	_, err := NewIndexer()
	assert.Nil(t, err)
}

func TestUID(t *testing.T) {

	indexer, _ := NewIndexer()

	expected := IndexerUID
	got := indexer.UID()
	assert.Equal(t, expected, got)
}

func TestFetchValid(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	testcases := []struct {
		url            string
		respStatusCode int
		respBody       []byte // byte slice here as typically this data will be loaded from a file using ioutil.ReadFile
	}{
		{
			url:            "http://example.com/thing",
			respStatusCode: http.StatusOK,
			respBody:       []byte("valid-data"),
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.url,
				httpmock.NewBytesResponder(testcase.respStatusCode, testcase.respBody),
			),
		)

		bytes, err := indexer.Fetch(context.Background(), testcase.url, client)
		if assert.NoError(t, err) {
			assert.Equal(t, testcase.respBody, bytes)
		}
		assert.Nil(t, httpmock.AllStubsCalled())
	}
}

func TestFetchInvalid(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	testcases := []struct {
		url            string
		respStatusCode int
		respBody       string
	}{
		{
			url:            "http://example.com/thing",
			respStatusCode: http.StatusNotFound,
			respBody:       "valid-data",
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.url,
				httpmock.NewStringResponder(testcase.respStatusCode, testcase.respBody),
			),
		)

		_, err := indexer.Fetch(context.Background(), testcase.url, client)
		assert.Error(t, err)
	}
	assert.Nil(t, httpmock.AllStubsCalled())
}

func TestParseValid(t *testing.T) {
	indexer, _ := NewIndexer()
	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	testcases := []struct {
		url   string
		bytes []byte
	}{
		{
			url:   "http://example.com/thing",
			bytes: []byte("valid-data"),
		},
	}

	for _, testcase := range testcases {
		_, err := indexer.Parse(testcase.bytes, testcase.url, timeProvider)
		assert.Nil(t, err)
	}
}

func TestParseInValid(t *testing.T) {
	indexer, _ := NewIndexer()
	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	testcases := []struct {
		url   string
		bytes []byte
	}{
		{
			url:   "http://example.com/thing",
			bytes: []byte("invalid-data"),
		},
	}

	for _, testcase := range testcases {
		_, err := indexer.Parse(testcase.bytes, testcase.url, timeProvider)
		assert.Error(t, err)
	}
}

func TestFetchResponseError(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	testcases := []struct {
		url            string
		respStatusCode int
		expected       error
	}{
		{
			url:            "http://example.com/thing",
			respStatusCode: http.StatusNotFound,
			expected:       thingfulx.ErrNotFound,
		},
		{
			url:            "http://example.com/thing",
			respStatusCode: http.StatusRequestTimeout,
			expected:       thingfulx.NewErrUnexpectedResponse("408"),
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.url,
				httpmock.NewBytesResponder(testcase.respStatusCode, []byte{}),
			),
		)

		ctx := context.Background()
		_, err := indexer.Fetch(ctx, testcase.url, client)
		if assert.Error(t, err) {
			assert.Equal(t, testcase.expected, err)
		}
	}
}

func TestFetchError(t *testing.T) {
	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	testcases := []string{"", "%"}

	for _, url := range testcases {
		_, err := indexer.Fetch(context.Background(), url, client)
		assert.Error(t, err)
	}
}

func TestURLS(t *testing.T) {
	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	expected := []string{"http://example.com/foo"}

	delay := time.Duration(0)

	got, err := indexer.URLS(context.Background(), client, delay)
	if assert.NoError(t, err) {
		assert.Equal(t, expected, got)
	}
}
